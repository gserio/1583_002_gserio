import java.util.Arrays; 
import java.util.Scanner;
import java.lang.String;


public class AdventureGame3
{
  public static Scanner input = new Scanner(System.in);
  public static char userChar;
  public static int currentRoom = 0;
  public static int loopVariable = 1;
  public static String[] stringArray = {"You have entered the Armory", "You have entered the Foyer", "You have entered the Library",
    "You have entered the Bed Room", "You have entered the Kitchen", "You have entered the Dining Room"};
  public static int movementArray[][] = {{3, -1, 1, -1}, {4, -1, 2, 0}, {5, -1, -1, 1}, {-1, 0, 4, -1}, {-1, 1, 5, 3}, {-1, 2, -1, 4}};
 
  public static void main (String [] args)
  {
    System.out.println("Enter Q or q at anytime to end the game");
    while(loopVariable>0)
    {
    roomDescription();
    if      ((userChar == 'N') || (userChar == 'n'))
         moveNorth();
    else if ((userChar == 'S') || (userChar == 's'))
         moveSouth();
    else if ((userChar == 'E') || (userChar == 'e'))
         moveEast();
    else if ((userChar == 'W') || (userChar == 'w'))
         moveWest();
    else if ((userChar == 'Q') || (userChar == 'q'))
      loopVariable = 0;
    }
    
    
  }
  
  public static void roomDescription()
  {
    switch (currentRoom)
    {
    case 0: 
      System.out.println(stringArray[0]);
      System.out.println("Exits are to the north and east");
      System.out.println("Which direction?");
      break;
    case 1: 
      System.out.println(stringArray[1]);
      System.out.println("Exits are to the west, north, and east");
      System.out.println("Which direction?");
      break;
    case 2:
      System.out.println(stringArray[2]);
      System.out.println("Exits are to the west and north");
      System.out.println("Which direction?");
      break;
    case 3: 
      System.out.println(stringArray[3]);
      System.out.println("Exits are to the south and east");
      System.out.println("Which direction?");
      break;
    case 4:
      System.out.println(stringArray[4]);
      System.out.println("Exits are to the west, south, and east");
      System.out.println("Which direction?");
      break;
    case 5: 
      System.out.println(stringArray[5]);
      System.out.println("Exits are to the west and south");
      System.out.println("Which direction?");
      break;
    }
      //take input for movement
    userChar = input.next().charAt(0);
  }
  
   
  public static void moveNorth()
  {
    if(movementArray[currentRoom][0] == -1)
         System.out.println("You cannot walk that way");
     else
         currentRoom = currentRoom + 3;    
  }

  public static void moveSouth()
  {
   if(movementArray[currentRoom][1] == -1)
         System.out.println("You cannot walk that way");
     else
         currentRoom -= 3; 
  }
  public static void moveEast()
  {
    if(movementArray[currentRoom][2] == -1)
         System.out.println("You cannot walk that way");
     else
         currentRoom += 1; 
  }
  public static void moveWest()
  {
    if(movementArray[currentRoom][3] == -1)
         System.out.println("You cannot walk that way");
     else
         currentRoom -= 1; 
  }
}//end main