import java.util.Scanner;
import java.security.SecureRandom;
import java.lang.String;

/*a game where the player creates a character and uses it to battle random1
monsters*/

public class ExpandedCombatSystem {
  
//initialize scanner
static public Scanner input = new Scanner(System.in);
//initialize random number generator
static public SecureRandom randomNumbers = new SecureRandom();

//initialize variables for hero attributes
static public int heroHealth;
static public int heroAttack;
static public int heroMagic;
static public int heroStats = 20;
static public int heroExperience;
static public int getStatsFromExp = 0;

//initialize variables of monster for name, health, attack power, experience point value
public static String monsterName = "";
static public int monsterHealth;
static public int monsterAttack;
static public int monsterExperience;

static public int choice;
static public int numberGenerated = 1 + randomNumbers.nextInt(3);

public static void main(String [] args)
{ 
  int loopProgram = 1;
  
  
  do
  {   
  levelUp();
  createHero();
  createMonster();
  
  do 
  {
  displayStatus();
  
  choice = input.nextInt();
  switch (choice)
  {
    case 1:
      melee();
      break;
    case 2:
      castSpell();
      break;
    case 3:
      chargeMana();
      break;
    case 4:
      runAway();
      break;
  }
  
  } while ((heroHealth >= 1) && (monsterHealth >= 1));
      
  } while (loopProgram == 1);
  
  
  
  
}

  



//method for allocating stats to the hero
private static void createHero()
{
  //resets hero stats for new game
  heroHealth = 0;
  heroAttack = 0;
  heroMagic = 0;
  heroStats = 20 + getStatsFromExp;
  
  do
  {  
    //print out how many stats to allocate
    System.out.println("Health: " +heroHealth+ ", Attack: " +heroAttack+ ", Magic: " +heroMagic);
    System.out.println("1) +10 Health");
    System.out.println("2) +1 Attack");
    System.out.println("3) +3 magic");
    System.out.print("You have " +heroStats+ " points to spend: ");
   
    
    choice = input.nextInt();
     switch (choice) 
     {
       case 1:
        
         heroHealth += 10;
         heroStats -= 1;
         break;
  
       case 2: 
    
         heroAttack += 1;
         heroStats -= 1;
         break;
         
       case 3:
         
         heroMagic += 3;
         heroStats -= 1;
         break;
         
       default:
         
        System.out.println("Please provide the proper input");
        break;
      }//end switch statement
   } while (heroStats >= 1);      //end of do loop
}//end method for creating a hero
  
  
  
  
  
  
//method for generating random monster using switch statement

  
private static void createMonster() 
{  
  numberGenerated = 1 + randomNumbers.nextInt(3);
  //use random numberGenerated to create a goblin, orc, or troll
    switch (numberGenerated)
    { 
      case 1: 
      
      monsterName = "Goblin";
      monsterHealth = randomNumbers.nextInt(24) + 75;
      monsterAttack = randomNumbers.nextInt(4)+ 8;
      monsterExperience = 1;
      System.out.println("You encountered a " +monsterName);
      break;
      
      case 2: 
      
       monsterName = "Orc";
      monsterHealth = randomNumbers.nextInt(24) + 100;
      monsterAttack = randomNumbers.nextInt(4) + 15;
      monsterExperience = 3;
      System.out.println("You encountered an " +monsterName); 
      break;
      
      case 3:
      
      monsterName = "Troll";
      monsterHealth = randomNumbers.nextInt(49) + 150;
      monsterAttack = randomNumbers.nextInt(4) + 15;
      monsterExperience = 5;
      System.out.println("You encountered a "+monsterName);
      break;
    }    
}//end method for creating a random monster


    
 //method for displaying combat status 
  private static void displayStatus()
  {
    
   System.out.println(monsterName+ ": " + "HP: " +monsterHealth);
   System.out.println("Hero: HP: " + heroHealth + " MP: " + heroMagic);
   System.out.println("1) Sword Attack");
   System.out.println("2) Cast Spell");
   System.out.println("3) Charge Mana");
   System.out.println("4) Run Away");
   
   
   
   
  }
   
 private static void melee()
 { 
   monsterHealth = monsterHealth - heroAttack;
   if (monsterHealth <= 0)
   {
     System.out.println("You killed the " +monsterName+ " !");
     heroExperience = heroExperience + monsterExperience;
   }
   if (monsterHealth >= 1)
   {  
   heroHealth = heroHealth - monsterAttack;
   System.out.println("The " +monsterName+ " attacked you for " +monsterAttack+ " damage");
      
       if (heroHealth <= 0)
         System.out.println("You have died!");
   }
   
 }  
 
 private static void castSpell()
 {
   if (heroMagic >= 3)
   {
   monsterHealth = monsterHealth/2;
   heroMagic -= 3;
   System.out.println("You cast a spell on the " +monsterName);
   heroHealth = heroHealth - monsterAttack;
   System.out.println("The " +monsterName+ " attacked you for " +monsterAttack+ " damage");
   if (heroHealth <= 0)
         System.out.println("You have died!");
   }
   else
     System.out.println("You dont have enough mana");
 }
 
 private static void chargeMana()
 {
   heroMagic = heroMagic + 1;
   System.out.println("You charge your mana");
   heroHealth = heroHealth - monsterAttack;
   System.out.println("The " +monsterName+ " attacked you for " +monsterAttack+ " damage");
   if (heroHealth <= 0)
         System.out.println("You have died!");
 }
 
 private static void runAway()
 { 
   heroHealth = 0;
   monsterHealth = 0;
   System.out.println("You have fled");
 }
 
 private static void levelUp()
 {
   if (heroExperience >= 10)
   { 
     getStatsFromExp = getStatsFromExp + 5;
     System.out.println("You have leveled up!");
   }  
 }
}//end class