public class Dungeon{
Room celarium;
Room kitchen;


public void Dungeon(){
this.celarium = new Room("The sun shines, illuminating the whole room");
celarium.setExitDescription("The exit is to the north");
celarium.setNorth(kitchen);
this.kitchen = new Room("A bunch of pots and pans");
kitchen.setExitDescription("The exit is to the south");
kitchen.setSouth(celarium);
}
public Room getRoom0(){
  return celarium;
}
}

