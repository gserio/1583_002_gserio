public class Healthpack {
  
  private int health;
  
  public Healthpack(int health){
    this.health = health;
  }
  
  public int getHealth(){
    return health;
  }
  
  public String toString(){
    return "" + health;
  }
}