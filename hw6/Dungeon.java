public class Dungeon{
Room celarium;
Room hallway1;
Room processingRoom;
Room hallway2;
Room courtyard;
Room hallway3;
Room guardRoom;
Room hallway4;
Room showerRoom;
Room hallway5;
Room experimentRoom;
Room hallway6;
Room furnaceRoom;
Room hallway7;
Room workRoom;
Room hallway8;
Room prisonerRoom;
Monster myMonster;
Hero myHero;
Healthpack myHealthPack;

public Dungeon(){
myMonster = new Monster("Der Fuhrer", 100, 10);
myHero = new Hero(100, 40);
myHealthPack = new Healthpack(50);

  
this.celarium = new Room("The sun shines, illuminating the whole room");
celarium.setExitDescription("The exit is to the east");

this.hallway1 = new Room("You're in a hallway");
hallway1.setExitDescription("The exits are to the east and west");


this.processingRoom = new Room("Prisoner's come here have their ID number tattood on their arm");
processingRoom.setExitDescription("The exits are to the west and north");

this.hallway2 = new Room("Just another hallway");
hallway2.setExitDescription("The exits are to the north and south");

this.courtyard = new Room("A beautiful courtyard filled with people smoking fragrant cigars");
courtyard.setExitDescription("The exits are to the north, south, east, and west");

this.hallway3 = new Room ("Yet another hallway");
hallway3.setExitDescription("The exits are to the east and west");

this.guardRoom = new Room("The masters of you fate reside here");
guardRoom.setExitDescription("The exits are to the north and east");

this.hallway4 = new Room("How many hallways are there?");
hallway4.setExitDescription("The exits are to the north and south");

this.showerRoom = new Room("Here is the ''shower room''");
showerRoom.setExitDescription("The exit is to the south");

this.hallway5 = new Room("hallways are cool huh?");
hallway5.setExitDescription("The exits are to the north and south");

this.experimentRoom = new Room("Lots are useful scientific research is done here.... it just involves human test subjects");
experimentRoom.setExitDescription("The exits are to the east and south");

this.hallway6 = new Room("im running out of hallway description");
hallway6.setExitDescription("The exits are to the east and west");

this.furnaceRoom = new Room("This is the furnace room; I think the prisoners would prefer a finance room");
furnaceRoom.setExitDescription("There is no exit from here... jk go west");

this.hallway7 = new Room("I really can't come up with anymore descriptions for a hallway, use your imagination");
hallway7.setExitDescription("The exits are to the east and west");

this.workRoom = new Room("Arbeit macht frei!  What are you, lazy?");
workRoom.setExitDescription("The exits are to the west and south");

this.hallway8 = new Room("HAAAALLLLWAAAYS");
hallway8.setExitDescription("The exits are to the north and south");

this.prisonerRoom = new Room("People with a natural inclination towards finance... now furnace!");
prisonerRoom.setExitDescription("The exit is to the north");

celarium.setExits(null, null, hallway1, null);
hallway1.setExits(null, null, processingRoom, celarium);
processingRoom.setExits(hallway2, null, null, hallway1);
hallway2.setExits(courtyard, processingRoom, null, null);
courtyard.setExits(hallway5, hallway2, hallway7, hallway3);
hallway3.setExits(null, null, courtyard, guardRoom);
guardRoom.setExits(hallway4, null, hallway3, null);
hallway4.setExits(showerRoom, guardRoom, null, null);
showerRoom.setExits(null, hallway4, null, null);
hallway5.setExits(experimentRoom, courtyard, null, null);
experimentRoom.setExits(null, hallway5, hallway6, null);
hallway6.setExits(null, null, furnaceRoom, experimentRoom);
furnaceRoom.setExits(null, null, null, hallway6);
hallway7.setExits(null, null, workRoom, courtyard);
workRoom.setExits(null, hallway8, null, hallway7);
hallway8.setExits(workRoom, prisonerRoom, null, null);
prisonerRoom.setExits(hallway8, null, null, null);



}
public Room getRoom0(){
  return this.celarium;
}

public Monster getMonster(){
  return this.myMonster;
}

public Hero getHero(){
  return this.myHero;
}

public Healthpack getHealthPack(){
  return this.myHealthPack;
}

}

