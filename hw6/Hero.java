public class Hero{
  private int attack;
  private int health;
  
  public Hero(int health, int attack){
    this.attack = attack;
    this.health = health;
  }
  
  public int getAttack(){
    return attack;
  }
  
  public int getHealth(){
    return health;
  }
  
  public void setHealth(int health){
    this.health = health;
  }
  
  public String toString(){
    return "Your hero's attack: " + attack + "HP: " + health;
  }
}