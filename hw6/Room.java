public class Room
{
  private Room north;
  private Room south;
  private Room east;
  private Room west;
  
  
  private String roomDescription;
  private String exits;
  
  public Room(String roomDescription)
  {
    this.roomDescription = roomDescription;
  }
 
  public void setNorth(Room north){
    this.north = north;
  }
  
  public void setSouth(Room south){
    this.south = south;
  }
  
  public void setEast(Room east){
    this.east = east;
  }
  
  public void setWest(Room west){
    this.west = west;
  }
      
  public Room getNorth(){
    return this.north;
  }
  
  public Room getSouth(){
    return this.south;
  }
  
  public Room getWest(){
    return this.west;
  }
  
  public Room getEast(){
    return this.east;
  }
  
  public void setExits(Room north, Room south, Room east, Room west){
    this.north = north;
    this.south = south;
    this.east = east;
    this.west = west;
  }  

  public String getDescription(){
    return this.roomDescription;
  }
  
  public void setExitDescription(String exits)
  {
    this.exits = exits;
  }
  
  /*public String getExits(String exits){
    return this.exits;
}*/
  
  public String toString(){
    return roomDescription + "\n" + exits;
  }
}