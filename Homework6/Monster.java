public class Monster{
  
  private int health;
  private int attack;
  private String monsterName;
  
  public Monster(String monsterName, int health, int attack)
  {
    this.monsterName = monsterName;
    this.health = health;
    this.attack = attack;
  }
  
  public int getHealth(){
    return health;
  }
  
  public void setHealth(int health){
    this.health = health;
  }
    
  
  public int getAttack(){
    return attack;
  }
  
  public String getName(){
    return monsterName;
  }
  
  @Override
  public String toString(){
    return monsterName + " appears! Health: " + health;
  }
  
}