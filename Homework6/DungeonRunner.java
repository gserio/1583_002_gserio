import java.util.Scanner;
import java.lang.String; 
public class DungeonRunner{
  
  public static int i = 0;
  public static Hero coolguy;
  public static Monster baddie;
  public static Room currentRoom;
  public static Dungeon myDungeon = new Dungeon();
  public static Scanner input = new Scanner(System.in);
  public static String userString;
  public static int looper = 1;
  public static Healthpack myHealth;
  
  
  public static void main(String [] args){
    currentRoom = myDungeon.getRoom0();
    coolguy = myDungeon.getHero();
    baddie = myDungeon.getMonster();
    myHealth = myDungeon.getHealthPack();
    
    while(looper > 0){
    retrieveHealth();
    combatMethod();
    printRoomDescription();
    switchRoom(currentRoom, userString);    
    }
    
}//end main
  
  public static void printRoomDescription(){
    System.out.println(currentRoom);
  }//end printRoomDescription
  
  //method for switching rooms
  public static void switchRoom(Room wow, String userString){
    userString = input.nextLine();
    if((userString.equals("n") || (userString.equals("N"))) && (currentRoom.getNorth() != null))
      currentRoom = wow.getNorth();
    else if((userString.equals("S") || (userString.equals("s"))) && (currentRoom.getSouth() != null))
      currentRoom = wow.getSouth();
    else if((userString.equals("E") || (userString.equals("e"))) && (currentRoom.getEast() != null))
      currentRoom = wow.getEast();
    else if((userString.equals("W") || (userString.equals("w"))) && (currentRoom.getWest() != null))
      currentRoom = wow.getWest();
    else if((userString.equals("Q")) || (userString.equals("q"))){
      looper = 0;
    System.out.println("Thanks for playing mah dude!");
    }
    else
      System.out.println("You can't go that way retard, learn to read!");
  }
  
  public static void combatMethod(){
     Room [] roomArray = {myDungeon.hallway1, myDungeon.hallway2,
          myDungeon.hallway3, myDungeon.hallway4, myDungeon.hallway5};
     
     if(currentRoom == roomArray[i])
    {
      i++;
      while((coolguy.getHealth() > 0) && (baddie.getHealth() > 0))
      {
      System.out.println(baddie);
      System.out.println(coolguy);
      System.out.println("You have to fight, press A to attack");
      String choice = input.nextLine();
      if((choice.equals("A")) || (choice.equals("a"))){
        baddie.setHealth(baddie.getHealth() - coolguy.getAttack());
        coolguy.setHealth(coolguy.getHealth() - baddie.getAttack());
        if (baddie.getHealth() <= 0){
          System.out.println("Good job! I mean it wasn't that hard was it?");
           
        }
          
      }
      
      else
        System.out.println("Cmon dude just fight!");
      }//end while    
      baddie.setHealth(100);
      if(i >= 4)
        i = 0;
  }
 }//end combat method
  
    public static void retrieveHealth(){
      if (currentRoom == myDungeon.experimentRoom){
          coolguy.setHealth(coolguy.getHealth()+ myHealth.getHealth());
        System.out.println("You found a healthpack, restoring " + myHealth.getHealth() + " health!");
      }
      
     
    }//end retriveHealth()
    
}//end class
